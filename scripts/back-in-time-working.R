rm(list = ls()) 
setwd("~/Documents/back-in-time-analysis/production_painting_scripts")
panel = "Europe"


#### painting container 
cpvectorfile = "~/Documents/back-in-time-analysis/production_output_martin_adam/painting_results_example/LD0100042A_Europe/paintingvector.out"
historyfile <- "~/Documents/back-in-time-analysis/painting_panel_1.1/Europe/Europe/admixturemodel/historynames.txt"
displayfile = "~/Documents/back-in-time-analysis/painting_panel_1.1/Europe/Europe/admixturemodel/DisplayName.csv"
pointdensity <- "~/Documents/back-in-time-analysis/painting_panel_1.1/Europe/Europe/admixturemodel/pointdensity.RData" ## Contains: allpops,allmapregioncounts,allmappops
inmodel <- "~/Documents/back-in-time-analysis/painting_panel_1.1/Europe/Europe/admixturemodel/finaladmixres.RDS"
admixres <- readRDS(inmodel)



 #### production container
cpvectorfile = "~/Documents/back-in-time-analysis/production_painting_scripts/production_version/paintingvector.out"
historyfile <- "~/Documents/back-in-time-analysis/production_painting_scripts/Europe/admixturemodel/historynames.txt"
displayfile = "~/Documents/back-in-time-analysis/production_painting_scripts/Europe/admixturemodel/DisplayName.csv"
pointdensity <- "~/Documents/back-in-time-analysis/production_painting_scripts/Europe/admixturemodel/pointdensity.RData" ## Contains: allpops,allmapregioncounts,allmappops
inmodel <- "~/Documents/back-in-time-analysis/production_painting_scripts/Europe/admixturemodel/finaladmixres.RDS"
admixres <- readRDS(inmodel)


# making out own paiting vector ( read in everyting else for painting container )
sampledir<-"/Users/marinavabistsevits/Documents/back-in-time-analysis/production_output_martin_adam/painting_results_example/LD0100042A_Europe/"
donorfile<-read.table("/Users/marinavabistsevits/Documents/back-in-time-analysis/painting_panel_1.1/Europe/Europe/admixturemodel/donorids_usedonly.txt", header=F)
source("makePaintingVector.R")
paintingvector<- makePaintingVector(sampledir,donorfile)


simple = FALSE
verbose=TRUE
if (! simple) {
  tmp = load(pointdensity)
  if (verbose) print(paste("READING history details from ", historyfile))
  historykeep <- read.table(historyfile, as.is = T)
  historykeep = historykeep[order(historykeep[, 1]),]
  if (verbose) print(paste("Reading display names from", displayfile))
  displaytab = read.csv(displayfile, as.is = T)
  displaynames = displaytab[, 2]
}




if (verbose) print("Going back in time")

# main back in time functions
my.read.headed.table = function(x, ...){
  theader = read.table(file = x, header = F, skip = 0, nrow = 1, row.names = 1, as.is = T)
  ret = read.table(file = x, header = F, skip = 1, row.names = 1, ...)
  colnames(ret) = theader
  ret
}
getPopSharingRef <- function(targetlist, sharelist){
  sharemat <- sapply(names(sharelist), function(tpop){
    sapply(1 : length(targetlist), function(xidx){
      refx <- targetlist[[xidx]]
      tmp <- sharelist[[tpop]][[xidx]]
      sum(tmp * refx)
    })
  })
  colnames(sharemat) <- names(sharelist)
  sharemat
}
getSampleAdmixture <- function(sampledataraw, admixhist){
  
  #sampledataraw<-rawsamplemat
  # admixhist<-admixres$history
  
  ret = list()
  for (i in 1 : (length(admixhist) - 1)) { # 1:56
    # 62 (not changing)       # 62 (not changing either)
    if (dim(admixhist[[i]]$pop)[2] == dim(sampledataraw)[2]) {
      tdat = sampledataraw # only goes here
    }else {
      stop("The number of donors is inconsistent. ")
    }
    
    admixhist[[i]]$pop[is.na(admixhist[[i]]$pop)] = 0
    ret[[i]] <- admix.nnls(as.numeric(tdat),admixhist[[i]]$pop)
    
  }
  i = length(admixhist)
  ret[[i]] = 1
  names(ret[[i]]) = rownames(admixhist[[i]]$pop)
  return(ret)
}

## NLS related functions
admix.nnls<-function(X,Y){
  #X=as.numeric(tdat) # matrix 1x62 
  #Y=admixhist[[1]]$pop # matrix 57x62 , 56x62, ...
  
  ## Do an nnls admixture analysis (X ~ Y) where X is a vector
  if(class(X)!="numeric") stop("X must be numeric in admix.nnls")
  ourmix <- getoverallfit(Y,X)$x
  ourmix <- ourmix / sum(ourmix) # divide by 1
  names(ourmix)<-names(Y)
  ourmix
}
getoverallfit <- function(predmat, fitdata) {
  #fitdata = X # painting profile/vector of david
  #predmat = Y # matrix of painting profiles of all surrogates
  
  #restrict <- 1
  rep <- 1
  i <- 1
  while (rep == 1) {
    q <- getfit(predmat, fitdata, restrict = i)
    
    if(q$x[i]>0) {
      rep=0
    }
    i=i+1
  }
  return(q)
}
getfit <- function(predmat,fitdata,restrict=1){
  ##Gets the nnls fit with the restriction that a given row is not used.
  ##This addresses the sum to one constraint.
  ## If we succeed in getting a valid return, it is also guarenteed to be the
  ## best fitting. Also, we are guaranteed to get this from some restriction
  ## choice
  
  #fitdata = X # painting profile/vector of david
  #predmat = Y # matrix of painting profiles of all surrogates
  #restrict=1
  
  temp <- predmat[-restrict, ,drop=FALSE] # exclude 1 row 
  
  for (i in 1:nrow(temp)) { # 56 rows (1st iter)
    temp[i, ] <- temp[i, ] - predmat[restrict, ] # from pop2 subsract pop1
  }
  
  fitdata2 <- fitdata - predmat[restrict,] # substact the removed row from david vector
  
  v <- nnls(t(temp),fitdata2)
  
  x <- v$x # vector of 56
  newx <- 1:nrow(predmat)
  newx[!((1:nrow(predmat)) == restrict)] <- x
  newx[restrict] <- 1 - sum(x)
  v$x <- newx
  names(v$x) <- rownames(predmat)
  
  return(v)
}

## Point density functions
samplePositions = function(posl, n){
  ## Sample positions from a shape
  posl2 = do.call("rbind", posl)
  tsample = sample(1 : dim(posl2)[1], replace = TRUE, size = n, prob = posl2[, 3])
  posl2[tsample, 1 : 2, drop = FALSE] + cbind(runif(n, - 0.125, 0.125),
                                              runif(n, - 0.125, 0.125))
}
sampleAllPositions = function(allmappops, allpops, dlast, dthis, n0=100, w=1, thresh=0.05){
  ## Sample positions from a list of shapes
  ## Getting n0 on average from each, but weighted by w (minimum 1)
  ## Putting a threshold of thresh before doing the sampling
  ## The sampling is systematic by probability
  
  if (length(n0) == 1)n0 = rep(n0, length(dlast))
  if (length(w) == 1)weights = rep(w, length(dlast))
  n = n0 * w / mean(w)
  samplecdfs = lapply(n, function(tn){
    if (tn < 1) return(0.5)
    seq(thresh, 1, length.out = ceiling(tn))
  })
  t2 = sapply(1 : length(dthis), function(i){
    sum(samplecdfs[[i]] > dlast[i] & samplecdfs[[i]] <= dthis[i])
  })
  names(t2) = names(allpops)
  if (sum(t2) == 0) return(data.frame(x = numeric(), y = numeric(), name = character()))
  
  samplepos = lapply(which(t2 > 0), function(i){
    cbind(as.data.frame(samplePositions(allmappops[[names(t2)[i]]], t2[i])),
          name = names(t2)[i])
  })
  do.call("rbind", samplepos)
}
matToList=function(x){
  ## Converts a matrix into a list of vectors
  ret=list()
  for(i in 1:dim(x)[2]) ret[[i]]=x[,i,drop=T]
  names(ret)=colnames(x)
  ret
}



#######################
## Obtain the back-in-time history

set.seed(1)
rawsamplemat = my.read.headed.table(cpvectorfile)    # painting.vector
rawsamplemat = paintingvector
testfit <- getSampleAdmixture(rawsamplemat, admixres$history)
testps <- getPopSharingRef(testfit, admixres$sharinglist)


testpsii = apply(testps, 2, function(x) sapply(1 : length(x), function(i) max(x[1 : i])))
testpsii = testpsii[historykeep[, 1], colnames(testpsii) %in% displaynames]




## Make the points
if (verbose) print("Sampling relatives")

myhistory = testpsii
mynames = colnames(myhistory)[colnames(myhistory) %in% names(allpops)]
allpops = allpops[mynames]
hpoints = list()
testhistory = myhistory[, mynames]
testhistory[testhistory < 0.05] = 0
testhistory[, allpops == 0] = 0
hpoints[[1]] = sampleAllPositions(allmappops, allpops, rep(0, length(mynames)),
                                  testhistory[1,], w = allpops, thresh = 0.05)
hpoints[[1]] = unname(as.matrix(hpoints[[1]][, 1 : 2, drop = FALSE]), force = TRUE)
colnames(hpoints[[1]]) = c("x", "y")

for (i in 2 : dim(myhistory)[1]) {
  hpoints[[i]] = sampleAllPositions(allmappops, allpops,
                                    testhistory[i - 1,], testhistory[i,],
                                    w = allpops, thresh = 0.05)
  ## Strip the information we don't want
  hpoints[[i]] = unname(as.matrix(hpoints[[i]][, 1 : 2, drop = FALSE]), force = TRUE)
  colnames(hpoints[[i]]) = c("x", "y")
}




library(jsonlite)
retjson = toJSON(list(
  version = "version",
  panel = "EU",
  history = matToList(testpsii),
  historylist = historykeep[, 2],
  historypts = lapply(hpoints, function(x){unname(x[, 1 : 2])})
))

cat(retjson,file = paste0("testing_old_nnls_painting_myvector2",".json"))
